package com.hasselfeldt.notes;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;

public class NoteKey {

	public static NoteKey of(long timestampInMillis) {
		Instant instant = Instant.ofEpochMilli(timestampInMillis);
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(instant, ZoneId.of("Europe/Berlin"));
		return new NoteKey(dateTime);
	}

	private ZonedDateTime createdDateTime;

	public NoteKey(ZonedDateTime createdDateTime) {
		super();
		this.createdDateTime = Objects.requireNonNull(createdDateTime, "createdDateTime must not be null");
	}

	public ZonedDateTime getCreatedDateTime() {
		return createdDateTime;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(createdDateTime);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NoteKey other = (NoteKey) obj;
		return Objects.equals(createdDateTime, other.createdDateTime);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NoteKey [createdDateTime=" + createdDateTime + "]";
	}

}
