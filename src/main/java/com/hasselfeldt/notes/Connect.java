package com.hasselfeldt.notes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/*
 * CREATE TABLE categories (
 *  _id integer primary key autoincrement,
 *  color long,
 *  name text not null)
 *
 * CREATE TABLE sqlite_sequence(name,seq)
 *
 * CREATE TABLE notes (
 *  _id integer primary key autoincrement,
 *  luid long,
 *  body text not null,
 *  lastedited_date long,
 *  created_date long,
 *  categories text)
 *
 * CREATE TABLE android_metadata (locale TEXT)
 */

public class Connect {

	public static void main(String[] args) {
		String mpbPath = args[0];
		if (mpbPath == null || mpbPath.isEmpty()) {
			System.exit(1);
		}
		Connect app = new Connect();
		app.selectAllCategories(mpbPath);
		app.selectAllNotes(mpbPath);
		app.selectAllMetadata(mpbPath);
		app.selectAllSequences(mpbPath);
		System.exit(0);
	}

	/**
	 * Connect to a sample database
	 */
	public static Connection connect(String mpbPath) {
		Connection conn = null;
		try {
			// db parameters
			String url = "jdbc:sqlite:" + mpbPath;
			// create a connection to the database
			conn = DriverManager.getConnection(url);

			System.out.println("Connection to SQLite has been established.");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}

	/**
	 * select all rows in the categories table
	 */
	public void selectAllCategories(String mpbPath) {
		String sql = "SELECT _id, color, name FROM categories";

		try (Connection conn = connect(mpbPath); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getInt("_id") + "\t" + rs.getLong("color") + "\t" + rs.getString("name"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * select all rows in the notes table
	 */
	public void selectAllNotes(String mpbPath) {
		String sql = "SELECT _id, luid, body, lastedited_date, created_date, categories FROM notes";

		try (Connection conn = connect(mpbPath); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getInt("_id") + "\t" + rs.getLong("luid") + "\t" + rs.getString("body") + "\t"
								+ toFormatedDateTime(rs.getLong("lastedited_date")) + "\t"
								+ toFormatedDateTime(rs.getLong("created_date")) + "\t" + rs.getString("categories"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * select all rows in the android_metadata table
	 */
	public void selectAllMetadata(String mpbPath) {
		String sql = "SELECT locale FROM android_metadata";

		try (Connection conn = connect(mpbPath); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getString("locale"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * select all rows in the android_metadata table
	 */
	public void selectAllSequences(String mpbPath) {
		String sql = "SELECT name, seq FROM sqlite_sequence";

		try (Connection conn = connect(mpbPath); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getString("name") + "\t" + rs.getInt("seq"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	private String toFormatedDateTime(long timestampInMillis) {
		Instant instant = Instant.ofEpochMilli(timestampInMillis);
		// LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
		ZonedDateTime dateTime = ZonedDateTime.ofInstant(instant, ZoneId.of("Europe/Berlin"));
		ZonedDateTime inUTC = dateTime.withZoneSameInstant(ZoneId.of("UTC"));
		if (inUTC.isEqual(dateTime)) {
			System.out.println("equal");
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		String formattedDateTime = dateTime.format(formatter);
		return formattedDateTime;
	}
}
